import React, {useState} from 'react'
import ChildHookMouse from './ChildHookMouse'

function ParentHookMouse() {
  const [display, setDisplay] = useState(true)
  return (
    <div>
      <button onClick={() => setDisplay(!display)}>Toggle</button>
      {display && <ChildHookMouse />}
    </div>
  )
}

export default ParentHookMouse