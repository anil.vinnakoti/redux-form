import React, {useState, useEffect} from 'react'

function ChildHookMouse() {
  const [x, setX] = useState(0)  
  const [y, setY] = useState(0)  

  const mouseEvent = e => {
    console.log('mouse event');
    setX(e.clientX)
    setY(e.clientY)
  }

  useEffect(() => {
    console.log('effect called');
    window.addEventListener('mousemove', mouseEvent)

    return () => {
      window.removeEventListener('mousemove', mouseEvent)
    }
  },[])

  return (
    <div>
      X- {x} Y- {y}
    </div>
  )
}

export default ChildHookMouse