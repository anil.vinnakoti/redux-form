import React, {useState, useEffect} from 'react'
import axios from 'axios'

function DataFetch() {
  const [posts, setPosts] = useState([])

  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/users')
        .then(res => {
          console.log(res.data);
          setPosts(res.data)
        })
        .catch(err => {
          console.log(err);
        })
  }, [])

  return (
    <div>
      <ul>
        {posts.map(item => <li key={item.id}>{item.name}</li>)}
      </ul>
    </div>
  )
}

export default DataFetch