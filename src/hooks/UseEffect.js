import React, {useEffect, useState} from 'react'

function UseEffectDemo() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState('');

  useEffect(() => {
    console.log('updated');
    document.title = `Clicked ${count} times`
  }, [count])
  return (
    <div>
      <input type='text' value={name} onChange={(event) => setName(event.target.value) } />
      <button onClick={() => setCount((prevCount) => prevCount+1)}>Click</button>
    </div>
  )
}

export default UseEffectDemo