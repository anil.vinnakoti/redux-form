// An interesting mechanism of React, which is not mentioned much, is the state batch updating. 
// Instead of one by one, React does batch updates, reducing the number of component renders.

// The main idea is that no matter how many setState calls you make inside a React event handler or synchronous lifecycle method, 
// it will be batched into a single update. That is only one single re-render will eventually happen.

// Note that this functionality does not always work. 
// In an event handler that uses an asynchronous operation of different kinds, 
// such as async/await, then/catch, setTimeout, fetch, etc. Separate state updates will not be batched.

// Fortunately, we can overcome this by using ReactDOM.unstable_batchUpdate , 
// And still benefit from state batched updates as needed.

//example for async operations batch handling

// const handleClickThen = () => {
//   Promise.resolve().then(res => {
//     ReactDOM.unstable_batchedUpdates(() => {
//     setCount1(count1 + 1);
//     setCount2(count2 + 1);
//     setCounter3(count3 + 1);
//   });
// });
// }


//  https://medium.com/swlh/react-state-batch-update-b1b61bd28cd2

import React, {Component} from "react";

class BatchUpdate extends Component {
  state = {
    count1: 0,
    count2: 0,
    count3: 0,
  }

  clickHanlder = () => {
    this.setState((state) => {
      return {
        count1: state.count1 + 1,
        count2: state.count2 + 1,
        count3: state.count3 + 1
      }
    })
  }

  componentDidMount(){
    console.log('Component mounted');
  }

  componentDidUpdate(){
    console.log('Component updated');
  }

  render() {
    let count=0;
    return (
      <div>
        <div>Count1: {this.state.count1}</div>
        <div>Count2: {this.state.count2}</div>
        <div>Count3: {this.state.count3}</div>
        <button onClick={this.clickHanlder}>Increase Count</button>
      </div>
    );
  }
}

export default BatchUpdate