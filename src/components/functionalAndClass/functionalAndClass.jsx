// In class components, the render method will be called, whenever the state of the components changes. 
// On the other hand, the Functional components render the UI based on the props.

// Whenever we have the use case with the State of the component and rendering the UI based on the Component State, 
// use the Class Components

// Class Components should be preferred whenever we have the requirement with the state of the component.



// A Functional component is a function that takes props and returns JSX

// They do not have state or lifecycle methods.

// Functional components are easier to read, debug, and test. 
// They offer performance benefits, decreased coupling, and greater reusability.

// They with a few downsides … but I think the benefits strongly outweigh these. Use them whenever you can.

// Functional components are built into React.  You can get started adding them 
// (and refactoring unnecessary class components) to your existing projects right away!


// Conclusion

// To conclude, developers should use functional components as much as possible. 
// As functional components are easier to read, test, and have less code 
// (this is because they are JavaScript functions without state, lifecycle methods, 
  // and all other methods that come with state.) Thus, functional components are often referred to as presentational components. 
  // Ideally, you only use class components for when you need to use state and lifecycle methods. 



  //   https://medium.com/swlh/differences-between-react-class-components-vs-functional-components-2468e90f3857