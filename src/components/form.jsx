import React, { Component } from 'react';
import Select from 'react-select';
import {connect} from 'react-redux';
import { userData } from '../redux';


const mapStateToProps = (state) => {
  return {
    formData: state.formData
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    userData: (data) => dispatch(userData(data))
  }
}

const options = [
  { value:'software engineer', label: 'Software Enginer' },
  { value: 'hr', label: 'HR' },
  { value: 'manager', label: 'Manager' },
  { value: 'administrator', label: 'Administrator' }
];



class Form extends Component {
  state = {
    firstName:'',
    lastName:'',
    email:'',
    address:'',
    position:null,
    gender:'',
    checked:false,

  }

  inputHandler = (event) => {
    this.setState({
      [event.target.dataset.type] : event.target.value

    })
  }

  selectHandler = (event) => {
    this.setState({ 
      position:event
     });
  };

  checkHandler = () => {
    if(this.state.checked === false){
      this.setState({
        checked: true
      })
    }else{
      this.setState({
        checked: false
      })
    }
  }


  submitHandler = (event) => {
    event.preventDefault();
    const temp = {...this.state};
    this.props.userData(temp)
    this.setState({
      firstName:'',
      lastName:'',
      email:'',
      address:'',
      position:null,
      gender:'',
      checked:false,

    })

  }
  render() {
    return <div className='form-container'>
      <div className='field'>
        <label>First Name</label>
        <input onChange={this.inputHandler} data-type='firstName' placeholder='Enter first name' value={this.state.firstName} type="text" />
      </div>
      <div className='field'>
        <label>Last Name</label>
        <input onChange={this.inputHandler} data-type='lastName' placeholder='Enter last name' value={this.state.lastName} type="text" />
      </div>
      <div className='field'>
        <label>Email</label>
        <input onChange={this.inputHandler} data-type='email' placeholder='Enter email' value={this.state.email} type="text" />
      </div>
      <div className='field'>
        <label>Address</label>
        <input onChange={this.inputHandler} data-type='address' placeholder='Enter address' value={this.state.address} type="text" />
      </div>
      <div className='field'>
        <label>Position</label>
        <Select
        value={this.state.position}
        onChange={this.selectHandler}
        options={options}
        placeholder='Select a position'
      />


      </div>
      <div className='field radio'>
        <label >Gender</label>
        <div className='gender'>
          <div>
            <input onChange = {this.inputHandler}  data-type='gender' type="radio" value='male' checked={this.state.gender === 'male'} name='gender' value="male"/>
            <label >Male</label>
          </div>
          <div>
            <input onChange={this.inputHandler} data-type='gender' type="radio" value='female' checked={this.state.gender ==='female'} name='gender' value="female"/>
            <label >Female</label>
          </div>
        </div>
      </div>
      <div className='field check'>
        <div>
          <input type="checkbox" checked={this.state.checked} onChange={this.checkHandler} className="check" value={this.state.checked} />
          <span className="terms" >I have read the terms &amp; conditions.</span>
        </div>  
      </div>
      <div className='submit-button'>
        <button type='submit' onClick={this.submitHandler}>Submit</button>
      </div>
    </div>;
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (Form);

// http://react.tips/radio-buttons-in-reactjs/