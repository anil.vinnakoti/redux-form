import React, { Component } from 'react';

class Child extends Component {

  constructor(props) {
    super(props)
  
    this.state = { 
      count:this.props.count
     } 
  }

  render() { 
    console.log('child props count',this.props.count);
    console.log('child state count',this.state.count);
    return (
      <>
      <h1>
        {this.state.count}
      </h1>
      </>
    );
  }
}
 
export default Child;