import React, { Component } from 'react';
import Child from './child';

class Parent extends Component {
  state = { 
    count:5
   } 

   resetHandler = () => {
     this.setState({
       count:0
     }, () => console.log('parent state count',this.state.count))
   }
  render() { 
    return (
      <div>
        <Child count={this.state.count} />
        <button onClick={this.resetHandler}>Reset</button>
      </div>
    );
  }
}
 
export default Parent;