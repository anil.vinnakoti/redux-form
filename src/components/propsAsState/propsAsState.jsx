// the initial react state should not be set to a value of a reactive prop. 
// Why? Well because that state will not get changed unless you call the state setter, 
// a setState function. If the props from the upper level get changed, 
// the component will get the changed props, however, the state will stay the same as the initial prop value.
// This issue destroys the single source of truth concept used in components. It is a bad practice and it should be avoided.

// Where the props are used to set the initial state of the Component is a general anti-pattern of React. 
// This implies that the state of the component is tied to the props of the component. 
// The issue with doing this is that the constructor is only ever called once in the life cycle of the component.


// Solution

// This is an anti-pattern that should be avoided, 
// and the state should be updated in the componentWillReceiveProps lifecycle method instead. 
// That way the state and the props can stay in step with each other.

// class MyComponent extends Component {
//   constructor(props) {
//       super(props)
//       this.state = {
//           name: "",
//       }
//   }

//   componentWillReceiveProps(props) {
//       this.setState({
//           name: props.name,
//       })
//   }
// }


//  https://sentry.io/answers/using-props-to-initialize-state/#:~:text=Where%20the%20props%20are%20used,life%20cycle%20of%20the%20component.