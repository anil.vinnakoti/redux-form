import React, { Component } from 'react';


function Uncontrolled() {
  const nameRef = React.createRef();
  const emailRef = React.createRef();

  function onSubmit(event) {
    event.preventDefault();
    console.log("Name value: " + nameRef.current.value);
    console.log("Email value: " + emailRef.current.value);
  }

  function onChange(event) {
    console.log(nameRef.current.value);
  }
  return (
    <form onSubmit={onSubmit} onChange={onChange}>
      <input type="text" name="name" ref={nameRef} required />
      <input type="email" name="email" ref={emailRef} required />
      <input type="submit" value="Submit" />
    </form>
  );
}

export default Uncontrolled

// https://scriptverse.academy/tutorials/reactjs-defaultvalue-value.html

// Controlled vs. uncontrolled components: Key differences

// Now that we understand what React controlled and uncontrolled components are, 
// let’s review some key differences between them:

// Controlled components are predictable because the state of the form elements is handled by the component

// Uncontrolled components are not predictable because, during the lifecycle of a component, 
// the form elements can lose their reference and may be changed/affected by other sources

// Controlled components enable you to effectively employ form validation to your forms. 
// It doesn’t matter what changes the form elements. Their values are safe in our local states, 
// so that’s where we perform our validation

// With controlled components, you are very much in control of your form elements’ values. 
// You can dictate how they go and what can and cannot be inserted

// So which should you use in your React project? The question is not whether controlled are 
// uncontrolled components are better, but which better serves your use case and fits your personal preference. 
// Controlled components, obviously, afford you more control over your data, but if you’re more comfortable using uncontrolled components in your project, more power to you.



// There are no defined rules to help you determine when and how to use uncontrolled components 
// versus controlled components in React; it all depends on how much control you want to have over your inputs.