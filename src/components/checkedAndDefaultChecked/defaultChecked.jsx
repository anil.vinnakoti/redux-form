// In the React rendering lifecycle, the value attribute on form elements will override the value in the DOM. 
// With an uncontrolled component, you often want React to specify the initial value, 
// but leave subsequent updates uncontrolled. To handle this case, you can specify a defaultValue attribute instead of value. 
// Changing the value of defaultValue attribute after a component has mounted will not cause any update of the value in the DOM.


import React, { Component } from 'react'

class DefaultChecked extends Component {
  constructor(props) {
    super(props)
    this.male = React.createRef()
    this.female = React.createRef()
  }

  submitHandler = (event) => {
    event.preventDefault()
    console.log(this.male.current.value);
    console.log(this.female.current.value );
  }
  render() { 
    return (
      <form>
        <input ref={this.male} type='radio' name='gender' defaultValue='male' defaultChecked={false} />
        <label>Male</label>

        <input ref={this.female} type='radio' name='gender' defaultValue='female' defaultChecked={false}/>
        <label>Female</label>

        <input onClick={this.submitHandler} type='submit' value='Submit' />
      </form>
    );
  }
}

 
export default DefaultChecked;