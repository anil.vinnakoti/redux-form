// Default Value

// the initially assigned value example@example.com stays put! This is because, 
// in the lifecycle of React's rendering, the value attributes in the form elements overrides the values in the DOM.

//  https://scriptverse.academy/tutorials/reactjs-defaultvalue-value.html

import React, { Component } from 'react'

class DefaultValue extends React.Component {
  constructor(props) {
    super(props);
    this.input = React.createRef();
  }

    changeHandler = () => {
    console.log(this.input.current.value);
  }

  render() {
    return (
      <form onChange={this.changeHandler}>
          <input value="Something" type="input" ref={this.input} />
      </form>
    );
  }
}

export default DefaultValue;