import React, { Component } from 'react'
class Value extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       something:''
    }
  }

  changeHandler = (input) => {
    this.setState({
      something:input
    },() => console.log(this.state.something))
  }

  render() { 
    return (
      <div>
        <input type='text' value={this.state.something} onChange={(event)=>this.changeHandler(event.target.value)} />
      </div>
    );
  }
}
 
export default Value;