import './App.css';
import BatchUpdate from './components/batchUpdate/batch';
import DefaultChecked from './components/checkedAndDefaultChecked/defaultChecked';
import Form from './components/form';
import Parent from './components/propsAsState/parent';
import Uncontrolled from './components/uncontrolled/uncontrolled';
import DefaultValue from './components/valueAndDefaultValue/DefaultValue';
import Value from './components/valueAndDefaultValue/value';
import DataFetch from './hooks/DataFetch';
import ParentHookMouse from './hooks/ParentHookMouse';
import UseEffectDemo from './hooks/UseEffect';

function App() {
  return (
    <div className="App">
      {/* <Form /> */}
      {/* <Uncontrolled /> */}
      {/* <BatchUpdate /> */}
      {/* <Parent /> */}
      {/* <DefaultValue /> */}
      {/* <Value /> */}
      {/* <DefaultChecked /> */}
      {/* <UseEffectDemo /> */}
      {/* <ParentHookMouse /> */}
      <DataFetch />
    </div>
  );
}

export default App;
