import { USER_DATA } from '../actionTypes/actionTypes'

export const userData = (details) => {
  return {
    type: USER_DATA,
    payload: details
  }
}

