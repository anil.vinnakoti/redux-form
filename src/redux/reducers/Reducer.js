import { USER_DATA} from '../actionTypes/actionTypes';

const initialState = {
  formData: {},
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_DATA:
      return {
        ...state,
        formData: action.payload
      };

    default:
      return state
  }
}

export default userReducer